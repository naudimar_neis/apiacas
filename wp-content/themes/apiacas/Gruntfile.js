module.exports = function(grunt) {

    // 1. Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // 2. Config.
        watch: {
            compass: {
                files: ['assets/scss/**/*'],
                tasks: ['compass:css'],
                options: {
                  livereload: true,
                },
            },
            js: {
                files: ['assets/js/*.js', '!assets/js/production.js'],
                tasks: ['concat'],
                options: {
                  livereload: true,
                },
            },
        },
        compass: {
            css: {
                options: {
                    sassDir: ['assets/scss'],
                    cssDir: ['assets/css'],
                    sourcemap: true,
                    environment: 'development',
                }
            },
        },
        concat: {
            options: {
                separator: '',
                sourceMap: true,
            },
            js: {
                src: [
                    'assets/js/libs/jquery-1.12.3.min.js',
                    'assets/js/libs/bootstrap.min.js',
                    'assets/js/libs/slick.min.js',
                    'assets/js/libs/featherlight.js',
                    'assets/js/libs/featherlight.gallery.js',
                    'assets/js/libs/aos.js',
                    'assets/js/libs/gmaps.js',
                    'assets/js/main.js',
                ],
                dest: 'assets/js/production.js',
            },
        },
        uglify: {
            prod: {
                src: [
                  'assets/js/production.js',
                ],
                dest: 'assets/js/production.js'
            },
        },
        cssmin: {
          css:{
            src: 'assets/css/application.css',
            dest: 'assets/css/application.css',
          }
        },
    });

    // 3. Dependent plugins.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // 4. Tasks.
    grunt.registerTask('default', ['concat', 'watch']);
    grunt.registerTask('prod', ['concat', 'uglify', 'compass:css', 'cssmin']);

};
