

$(document).ready(function() {

  $('#banner, #banner-mobile').slick({
    dots: true,
    fade: true,
    arrows: true,
    cssEase: 'ease-in',
    prevArrow: '<button type="button" class="nave prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="nave next"><i class="fa fa-angle-right"></i></button>',
  });

  GMaps.geocode({
    address: $('#map').data('address'),
    callback: function(results, status) {
      if (status == 'OK') {
        var latlng = results[0].geometry.location;
        maps = new GMaps({
          div: '#map',
          lat: latlng.lat(),
          lng: latlng.lng()
        });
        maps.addMarker({
          lat: latlng.lat(),
          lng: latlng.lng()
        })
      }
    }
  });

  // slider de produto versão desktop
  $('#product-slider').slick({
    slidesToShow: 4,
    infinite: true,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="next"><i class="fa fa-angle-right"></i></button>',
    responsive: [{
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
      }
    }]
  });

  // slider de produto versão mobile
  $('#product-slider-mobile').slick({
    slidesToShow: 2,
    infinite: true,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="next"><i class="fa fa-angle-right"></i></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    }]
  });

  $('a.product').featherlightGallery({
    previousIcon: '<i class="fa fa-angle-left"></i>',
    nextIcon: '<i class="fa fa-angle-right"></i>',
    closeIcon: '<i class="fa fa-times"></i>',
    galleryFadeIn: 300,
    openSpeed: 300
  });

  // ao clicar no menu desktop
  $('#nav li a').click(function(){
    $('#nav li a').removeClass('selected');
    var div = $(this).attr('data-href');
    switch ($(this).attr('data-href')) {
      case '#banner':
          $('html, body').animate({scrollTop:$($(this).attr('data-href')).position().top-70}, 1000);
          break;
      case '#about':
          $('html, body').animate({scrollTop:$($(this).attr('data-href')).position().top-200}, 1000);
          break;
      case '#services':
          $('html, body').animate({scrollTop:$($(this).attr('data-href')).position().top-135}, 1000);
          break;
      case '#products':
          $('html, body').animate({scrollTop:$($(this).attr('data-href')).position().top-70}, 1000);
          break;
      default:

    }
    $(this).addClass('selected');
  })

  // ao clicar no menu mobile
  // ao clicar no menu desktop
  $('#bar1 ul li a').click(function(){
    $('#bar1').removeClass('in');
    $('#bar1 ul li a').removeClass('active');
    var div = $(this).attr('data-href');
    switch (div) {
      case '#banner-mobile':
          $('html, body').animate({scrollTop:$(div).position().top-70}, 1000);
          break;
      case '#about':
          $('html, body').animate({scrollTop:$(div).position().top-80}, 1000);
          break;
      case '#services':
          $('html, body').animate({scrollTop:$(div).position().top-135}, 1000);
          break;
      case '#products':
          $('html, body').animate({scrollTop:$(div).position().top-70}, 1000);
          break;
      default:

    }
    $(this).addClass('selected');
  })

  // ----------------------------------------------------------------------------------------
  // esse trecho de codigo é para corrigir o menu da pagina de orçamento
  // quando vem da página orçamento com um parametro p
  // o sistema verifica o tamanho do janela e faz o animate de acordo com o parametro
  // ----------------------------------------------------------------------------------------
    var p = getUrlParameter('p');
    if(p){
      // ------------------------------------------------------------------------------------
      // se for usar o menu para tamanho xs usa isso
      // ------------------------------------------------------------------------------------
      if ($(window).width() < 768) {
        switch (p) {
          case 'about':
              $('html, body').animate({scrollTop:$("#"+p).position().top-80}, 1000);
              break;
          case 'services':
              $('html, body').animate({scrollTop:$("#"+p).position().top-135}, 1000);
              break;
          case 'products':
              $('html, body').animate({scrollTop:$("#"+p).position().top-70}, 1000);
              break;
          default:
        }
      } else {
      // ------------------------------------------------------------------------------------
      // se for usar o menu para tamanho sm md lg usa isso
      // ------------------------------------------------------------------------------------
        switch (p) {
          case 'about':
              $('html, body').animate({scrollTop:$("#"+p).position().top-200}, 1000);
              break;
          case 'services':
              $('html, body').animate({scrollTop:$("#"+p).position().top-135}, 1000);
              break;
          case 'products':
              $('html, body').animate({scrollTop:$("#"+p).position().top-70}, 1000);
              break;
          default:

        }
      }
    }
  // ----------------------------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------
})

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : sParameterName[1];
      }
  }
};
