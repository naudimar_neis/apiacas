<section class="banner-container hidden-xs visible-sm visible-md visible-lg">
  <div id="banner">
    <?php while( has_sub_field('banners',  get_data_page('home', ID)) ): ?>
      <div class="item" style="background: url('<?php the_sub_field('background');?>') center center no-repeat;">
        <div class="container banner-content">
          <div class="row">
            <div class="col-sm-5 col-sm-offset-6 col-md-5 col-md-offset-6 col-lg-5 col-lg-offset-6">
              <h3><?php the_sub_field('title');?></h3>
              <p><?php the_sub_field('content');?></p>
              <?php if(get_sub_field('text-button')): ?>
                <a href="<?php the_sub_field('link-button');?>" title="<?php the_sub_field('text-button');?>" target="<?php the_sub_field('target');?>"><?php the_sub_field('text-button');?></a>
              <?php endif;?>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile;?>
  </div>
</section>
