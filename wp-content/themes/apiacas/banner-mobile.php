<section class="banner-container hidden-lg hidden-md hidden-sm visible-xs">
  <div id="banner-mobile">
    <?php while( has_sub_field('banners',  get_data_page('home', ID)) ): ?>
      <div class="item" style="background: url('<?php the_sub_field('background_mobile');?>') center center no-repeat;">
        <div class="container banner-content">
          <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
              <h3><?php the_sub_field('title');?></h3>
              <p><?php the_sub_field('content');?></p>
              <?php if(get_sub_field('text-button')): ?>
                <a href="<?php the_sub_field('link-button');?>" title="<?php the_sub_field('text-button');?>" target="<?php the_sub_field('target');?>"><?php the_sub_field('text-button');?></a>
              <?php endif;?>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile;?>
  </div>
</section>
