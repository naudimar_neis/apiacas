<?php

define('SITE', get_bloginfo('url').'/');
define('TEMA', get_bloginfo('template_url').'/');
define('CSS', get_bloginfo('template_url').'/assets/css/');
define('JS', get_bloginfo('template_url').'/assets/js/');
define('IMG', get_bloginfo('template_url').'/assets/img/');
define('VIDEO', get_bloginfo('template_url').'/assets/video/');

//add_image_size( 'diretor', 455, 456, true );

function addCustonPostType($qry, $singular, $plural, $icon = '',$suporte='',$gen='m'){
	if ($suporte=='') $suporte = array('title','editor','thumbnail','excerpt','comments');
	if ($gen == 'm') $nov = "novo"; elseif ($gen == 'f') $nov = "nova";
	if ($gen == 'm') $nem = "nenhum"; elseif ($gen == 'f') $nem = "nenhuma";
	$labels = array(
        'name' => _x(ucfirst($plural), 'post type general name'),
        'singular_name' => _x(ucfirst($singular), 'post type singular name'),
        'add_new' => _x(ucfirst($nov) . ' ' . $singular, 'Adicionar '.$singular),
        'add_new_item' => __('Adicionar ' . $nov . ' ' . $singular),
        'edit_item' => __('Editar ' . $singular),
        'new_item' => __(ucfirst($nov) . ' ' . $singular),
        'view_item' => __('Ver '.$singular),
        'search_items' => __('Procurar '.$singular),
        'not_found' =>  __($nem .' '.$singular.' encontrado'),
        'not_found_in_trash' => __($nem . ' ' .$singular.' encontrado no lixo'),
        'parent_item_colon' => '',
        'menu_name' => ucfirst($plural)
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => $suporte
    );
    if($icon) $args['menu_icon'] = $icon;
    register_post_type($qry,$args);
}
function addCustonTaxonomy($qry, $singular, $plural, $postType = array(),$gen='m'){
    if ($gen == 'm') $art = "Todos os "; elseif ($gen == 'f') $art = "Todas as ";
    $labels = array(
        'name' => _x( ucfirst($plural), 'taxonomy general name' ),
        'singular_name' => _x( ucfirst($plural), 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar '.$singular ),
        'all_items' => __( $art.$plural ),
        'parent_item' => __( $singular.' mestre' ),
        'parent_item_colon' => __( 'Parent Genre:' ),
        'edit_item' => __( 'Editar '.$singular ),
        'update_item' => __( 'Atualizar '.$singular ),
        'add_new_item' => __( 'Adicionar '.$singular ),
        'new_item_name' => __( 'Novo nome de '.$singular ),
        'menu_name' => __( ucfirst($plural) )
    );
    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'with_front' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => $qry ),
    );


    register_taxonomy($qry,$postType, $args);

}

// addCustonPostType('colecao','Coleção','Coleções', '', array('title','thumbnail'), 'f');


add_theme_support( 'post-thumbnails');


add_filter('show_admin_bar', '__return_false');
function my_login_logo() {
    print '
    <style type="text/css">
        body{
          background-color: #fff!important;
        }
        body.login div#login h1 a {
            background-image: url('.IMG.'logo.jpg);
            padding-bottom: 30px;
            background-size: 220px;
            width: 220px;
            height: 38px;
        }
        body.login form{
          box-shadow: 0 1px 3px rgb(0, 0, 0)
        }
    </style>';
}

add_action( 'login_enqueue_scripts', 'my_login_logo' );

// registra menu
function register_my_menu() {
  register_nav_menu( 'primary',  'Menu Principal' );
}
add_action( 'after_setup_theme', 'register_my_menu' );

add_action( 'login_enqueue_scripts', 'my_login_logo' );


//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


/**
* Desabilitando o protocolo xmlrpc para efitar ataques DDoS
*/
add_filter( 'xmlrpc_methods', function( $methods ) {
 unset( $methods['pingback.ping'] );
 return $methods;
} );


/**
 * Desabilitando o protocolo xmlrpc para efitar ataques DDoS
 */
add_filter( 'xmlrpc_methods', function( $methods ) {
  unset( $methods['pingback.ping'] );
  return $methods;
} );


function get_data_page($slug, $field){
  $data = get_page_by_path($slug);

  return $data->$field;
}
?>
