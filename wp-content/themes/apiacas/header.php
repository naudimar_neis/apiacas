<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
	<head>
		<script src="http://localhost:35729/livereload.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="cache-control" content="no-cache, must-revalidate" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="Mon, 26 Jul 1997 05:00:00 GMT" />


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyA1YV8kOAiEOEXFBVwfE4nQpMUGCZ53o"></script>
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700|Lato:300,400,700" rel="stylesheet">
    <link href="<?php echo CSS ?>application.css" rel="stylesheet" type="text/css" media="all" />
    <!-- <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" media="all" /> -->

    <!--[if IE]>
      <link href="assets/css/ie.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    <title>
      <?php
        global $page, $paged;

        wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
          echo " | $site_description";

        // Add a page number if necessary:
        if ( $paged >= 2 || $page >= 2 )
          echo ' | ' . sprintf( __( 'Page %s', 'nome_tema' ), max( $paged, $page ) );

      ?>
    </title>
    <?php if ( is_single() ) the_post(); ?>
    <meta name="description" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
      ?>
    "/>

    <link rel="shortcut icon" href="<?php echo TEMA ?>favicon.png" type="image/x-icon" />

    <meta name="author" content="PIPE Digital" />
    <meta name="description" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
     ?>
    " />
    <meta name="keywords" content="" />

    <link rel="canonical" href="<?php echo SITE ?>" />
    <meta property="og:title" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
      ?>
    "/>
    <meta property="og:type" content="" />
    <meta property="fb:app_id" content="">
    <meta property="og:url" content="<?php echo SITE ?>" />
    <meta property="og:image" content="<?php echo IMG ?>facebook.jpg">
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
     ?>
    ">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?php echo SITE ?>">
    <meta name="twitter:creator" content="PIPE Digital">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
     ?>
    ">

    <meta name="twitter:image" content="<?php echo IMG ?>twitter.jpg">

    <meta itemprop="name" content="">
    <meta itemprop="description" content="
      <?php
          if ( is_single() ) {
             echo get_the_excerpt();
          } else {
         bloginfo('name'); echo " - "; bloginfo('description');
          }
     ?>
    ">
    <meta itemprop="image" content="<?php echo IMG ?>facebook.jpg">

    <?php wp_head(); ?>

  </head>
	<body>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-96008772-1', 'auto');
      ga('send', 'pageview');

    </script>

  <nav>
    <div class="container">
      <div class="row hidden-xs">
        <div class="col-sm-4 col-md-3 col-lg-3">
          <a href="<?php echo SITE ?>" class="logo">
            <img src="<?php echo IMG ?>logo.jpg" alt="APIACÁS CALL CENTER" />
          </a>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-5">
          <ul id="nav" class="list-inline">
            <li><a <?php print (is_home()) ? 'data-href="#banner"' : 'href="'.SITE.'"';?> class="selected" title="HOME">HOME</a></li>
            <li><a <?php print (is_home()) ? 'data-href="#about"' : 'href="'.SITE.'?p=about"';?> title="QUEM SOMOS">QUEM SOMOS</a></li>
            <li><a <?php print (is_home()) ? 'data-href="#services"' : 'href="'.SITE.'?p=services"';?> title="SERVIÇOS">SERVIÇOS</a></li>
            <li><a <?php print (is_home()) ? 'data-href="#products"' : 'href="'.SITE.'?p=products"';?> title="PRODUTOS">PRODUTOS</a></li>
          </ul>
        </div>
        <div class="col-md-1 col-lg-2 hidden-sm hidden-md">
          <a href="<?php echo SITE?>orcamento" title="Faça um orçamento" class="make-budget">Faça um orçamento</a>
        </div>
        <div class="col-md-1 col-lg-2 hidden-sm hidden-md">
          <span><i class="fa fa-phone"></i> <?php the_field('phone', get_data_page('contato', 'ID'));?></span>
        </div>
        <div class="nmp col-sm-2 hidden-xs hidden-lg">
          <a href="<?php echo SITE?>orcamento" title="Faça um orçamento" class="make-budget">Faça um orçamento</a>
          <span><i class="fa fa-phone"></i> <?php the_field('phone', get_data_page('contato', 'ID'));?></span>
        </div>
      </div>
      <div class="row hidden-sm hidden-md hidden-lg">
        <div class="col-xs-12">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo SITE ?>" class="logo">
              <img src="<?php echo IMG ?>logo.jpg" alt="APIACÁS CALL CENTER" />
            </a>
          </div>
        </div>
        <div class="container-fluid">
          <div class="collapse navbar-collapse" id="bar1">
            <ul class="nav navbar-nav">
              <li><a <?php print (is_home()) ? 'data-href="#banner-mobile"' : 'href="'.SITE.'"';?> title="HOME">HOME</a></li>
              <li><a <?php print (is_home()) ? 'data-href="#about"' : 'href="'.SITE.'?p=about"';?> title="QUEM SOMOS">QUEM SOMOS</a></li>
              <li><a <?php print (is_home()) ? 'data-href="#services"' : 'href="'.SITE.'?p=services"';?> title="SERVIÇOS">SERVIÇOS</a></li>
              <li><a <?php print (is_home()) ? 'data-href="#products"' : 'href="'.SITE.'?p=products"';?> title="PRODUTOS">PRODUTOS</a></li>
              <li><a href="<?php echo SITE?>orcamento">FAÇA UM ORÇAMENTO</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </nav>
