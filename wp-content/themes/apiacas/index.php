<?php include('header.php') ?>
  <!-- start banner-container -->
  <?php
  include('banner-desktop.php');
  include('banner-mobile.php');
  ?>
  <!-- end banner-container -->
  <!-- start about-container -->
  <?php $page = get_page_by_path( 'quem-somos' ); ?>
  <section class="about-container" id="quem-somos">
    <div id="about" class="container">
      <div class="row">
        <div class="col-md-7 col-md-offset-2 col-xs-10 col-xs-offset-1">
          <h2>Quem Somos</h2>
          <?php echo apply_filters('the_content', get_data_page('quem-somos', 'post_content')); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7 col-md-offset-2 col-xs-10 col-xs-offset-1">
          <h3>Marcas que trabalhamos</h3>
          <ul class="list-inline">
            <?php while( has_sub_field('marcas', get_data_page('quem-somos', 'ID')) ): ?>
              <li><i class="fa fa-angle-right"></i><?php the_sub_field('mark'); ?></li>
            <?php endwhile; ?>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- end about-container -->
  <!-- start services -->
  <section class="services-container" id="servicos">
    <div id="services" class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
          <h2><img src="<?php echo IMG?>icon-service.png" /> Serviços</h2>
          <ul>
            <?php while( has_sub_field('services', get_data_page('servicos', 'ID')) ): ?>
              <li>
                <i class="fa fa-angle-right"></i>
                  <strong><?php the_sub_field('service'); ?></strong>
                <span><?php the_sub_field('description'); ?></span>
              </li>
            <?php endwhile; ?>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- end servicess -->
  <!-- start products -->
  <section id="products" class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-2 col-xs-10 col-xs-offset-1">
        <h2><img src="<?php echo IMG?>icon-products.png" /> Produtos</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
        <div id="product-slider">
          <?php while(has_sub_field('products', get_data_page('produtos', 'ID'))): ?>
            <div class="item">
              <a class="product" href="<?php the_sub_field('image'); ?>">
                <img src="<?php the_sub_field('thumb'); ?>" class="img-responsive" alt="<?php the_sub_field('name'); ?>">
                <?php the_sub_field('name'); ?>
              </a>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </section>
  <!-- end products -->
  <!-- start map -->
  <section class="map-container">
    <div id="map" data-address="<?php the_field('address', get_data_page('contato', 'ID')); ?>"></div>
  </section>
  <!-- end map -->
  <!-- start contact -->
  <section id="contatct">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><i class="fa fa-envelope" aria-hidden="true"></i><span><?php the_field('email', get_data_page('contato', 'ID'));?></span></div>
        <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><i class="fa fa-phone" aria-hidden="true"></i><span><?php the_field('phone', get_data_page('contato', 'ID'));?></span></div>
        <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><i class="fa fa-whatsapp" aria-hidden="true"></i><span><?php the_field('whatsapp', get_data_page('contato', 'ID'));?></span></div>
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 hidden-xs"><a href="http://<?php the_field('facebook', get_data_page('contato', 'ID'));?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i><span><?php the_field('facebook', get_data_page('contato', 'ID'));?></span></a></div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><i class="fa fa-map-marker" aria-hidden="true"></i><span><?php the_field('address', get_data_page('contato', 'ID'));?></span></div>
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><i class="fa fa-clock-o" aria-hidden="true"></i><span><?php the_field('hours', get_data_page('contato', 'ID'));?></span></div>
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 hidden-sm hidden-md hidden-lg"><a href="http://<?php the_field('facebook', get_data_page('contato', 'ID'));?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i><span><?php the_field('facebook', get_data_page('contato', 'ID'));?></span></a></div>
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><a href="http://<?php the_field('facebook', get_data_page('contato', 'ID'));?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i><span><?php the_field('instagram', get_data_page('contato', 'ID'));?></span></a></div>
      </div>
    </div>
  </section>
  <!-- end contact -->
<?php include('footer.php') ?>
