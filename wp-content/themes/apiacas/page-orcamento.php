<?php include('header.php') ?>
  <!-- start orcamento-container -->
  <section class="orcamento-container">
    <div id="orcamento" class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
          <h2>Faça um Orçamento</h2>
          <span>ou entre em contato</span>
          <?php echo do_shortcode('[contact-form-7 id="57" title="Orçamento_copy"]'); ?>
        </div>
      </div>
    </div>
  </section>
  <!-- end orcamento-container -->
<?php include('footer.php') ?>
